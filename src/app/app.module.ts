import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';   //necessary for angular animation/material
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
// Reducer ng-rx
import { counterReducer } from './angular-basic/ng-rx/ng-rx1/counter.reducer';
import { postReducer} from './angular-basic/ng-rx/ng-rx2/reducer/post.reducer';
import { simpleReducer} from './angular-basic/ng-rx/ng-rx0/simple.reducer';
import { addCoinReducer } from './angular-basic/ng-rx/ng-rx3/reducer/blockchain.reducer';

//comoponent
import { AppComponent } from './app.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { AngularBasicModule } from './angular-basic/angular-basic.module';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { OrderListModule } from './order-list/order-list.module';
import { AuthModule } from './auth/auth.module';
import { CoreModule } from './core/core.module';




@NgModule({
  declarations: [AppComponent],  //component/pipe/directive names
  imports: [                     //all modules name
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    SharedModule,
    OrderListModule,
    AuthModule,
    CoreModule,
    DashboardModule,
    AngularBasicModule,   //necessary for angular animation/material
    BrowserAnimationsModule,
    StoreModule.forRoot({     //ng-rx 
      count: counterReducer,
      post: postReducer,
      message: simpleReducer,
      blockchain: addCoinReducer
    }),   //ng-store
    StoreDevtoolsModule.instrument({
      maxAge: 10 // number of states to retain
    })
  ],
  providers: [],                  //service provider name
  bootstrap: [AppComponent],      //starting app component  where <app-root> is available
})
export class AppModule { }
