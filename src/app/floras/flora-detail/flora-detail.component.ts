import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';

import {Flora} from '../flora.model';
import {FloraService} from '../flora.service';

@Component({
  selector: 'app-flora-detail',
  templateUrl: './flora-detail.component.html',
  styleUrls: ['./flora-detail.component.css'],
})
export class FloraDetailComponent implements OnInit {
  flora: Flora;
  id: number;

  constructor(
    private floraService: FloraService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = +params['id'];
      this.flora = this.floraService.getFlora(this.id);
    });
  }

  onAddToOrderList() {
    this.floraService.addIngredientsToOrderList(this.flora.ingredients);
  }

  onEditFlora() {
    this.router.navigate(['edit'], {relativeTo: this.route});
  }

  onDeleteFlora() {
    this.floraService.deleteFlora(this.id);
    this.router.navigate(['/floras']);
  }
}
