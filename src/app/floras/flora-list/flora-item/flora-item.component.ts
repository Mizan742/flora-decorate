import {Component, OnInit, Input} from '@angular/core';

import {Flora} from '../../flora.model';

@Component({
  selector: 'app-flora-item',
  templateUrl: './flora-item.component.html',
  styleUrls: ['./flora-item.component.css'],
})
export class FloraItemComponent implements OnInit {
  @Input()
  flora: Flora;
  @Input()
  index: number;

  ngOnInit() {}
}
