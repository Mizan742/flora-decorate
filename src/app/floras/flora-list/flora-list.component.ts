import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';

import {Flora} from '../flora.model';
import {FloraService} from '../flora.service';

@Component({
  selector: 'app-flora-list',
  templateUrl: './flora-list.component.html',
  styleUrls: ['./flora-list.component.css'],
})
export class FloraListComponent implements OnInit, OnDestroy {
  floras: Flora[];
  subscription: Subscription;

  constructor(
    private floraService: FloraService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.subscription = this.floraService.florasChanged.subscribe(
      (floras: Flora[]) => {
        this.floras = floras;
      }
    );
    this.floras = this.floraService.getFloras();
  }

  onNewFlora() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
