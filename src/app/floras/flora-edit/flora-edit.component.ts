import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {FormGroup, FormControl, FormArray, Validators} from '@angular/forms';

import {FloraService} from '../flora.service';

@Component({
  selector: 'app-flora-edit',
  templateUrl: './flora-edit.component.html',
  styleUrls: ['./flora-edit.component.css'],
})
export class FloraEditComponent implements OnInit {
  id: number;
  editMode = false;
  floraForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private floraService: FloraService,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = +params['id'];
      this.editMode = params['id'] != null;
      this.initForm();
    });
  }

  onSubmit() {
    // const newFlora = new Flora(
    //   this.floraForm.value['name'],
    //   this.floraForm.value['description'],
    //   this.floraForm.value['imagePath'],
    //   this.floraForm.value['ingredients']);
    if (this.editMode) {
      this.floraService.updateFlora(this.id, this.floraForm.value);
    } else {
      this.floraService.addFlora(this.floraForm.value);
    }
    this.onCancel();
  }

  onAddIngredient() {
    (<FormArray>this.floraForm.get('ingredients')).push(
      new FormGroup({
        name: new FormControl(null, Validators.required),
        amount: new FormControl(null, [
          Validators.required,
          Validators.pattern(/^[1-9]+[0-9]*$/),
        ]),
      })
    );
  }

  onDeleteIngredient(index: number) {
    (<FormArray>this.floraForm.get('ingredients')).removeAt(index);
  }

  onCancel() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  getControls() {
    return (<FormArray>this.floraForm.get('ingredients')).controls;
  }

  private initForm() {
    let floraName = '';
    let floraImagePath = '';
    let floraDescription = '';
    let floraIngredients = new FormArray([]);

    if (this.editMode) {
      const flora = this.floraService.getFlora(this.id);
      floraName = flora.name;
      floraImagePath = flora.imagePath;
      floraDescription = flora.description;
      if (flora['ingredients']) {
        for (let ingredient of flora.ingredients) {
          floraIngredients.push(
            new FormGroup({
              name: new FormControl(ingredient.name, Validators.required),
              amount: new FormControl(ingredient.amount, [
                Validators.required,
                Validators.pattern(/^[1-9]+[0-9]*$/),
              ]),
            })
          );
        }
      }
    }

    this.floraForm = new FormGroup({
      name: new FormControl(floraName, Validators.required),
      imagePath: new FormControl(floraImagePath, Validators.required),
      description: new FormControl(floraDescription, Validators.required),
      ingredients: floraIngredients,
    });
  }
}
