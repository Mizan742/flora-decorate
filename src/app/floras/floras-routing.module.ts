import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {AuthGuard} from '../auth/auth-guard.service';
import {FloraEditComponent} from './flora-edit/flora-edit.component';
import {FloraDetailComponent} from './flora-detail/flora-detail.component';
import {FloraStartComponent} from './flora-start/flora-start.component';
import {FlorasComponent} from './floras.component';


const florasRoutes: Routes = [
  {
    path: '',
    component: FlorasComponent,
    children: [
      {path: '', component: FloraStartComponent},
      {path: 'new', component: FloraEditComponent, canActivate: [AuthGuard]},
      {path: ':id', component: FloraDetailComponent },
      {
        path: ':id/edit',
        component: FloraEditComponent,
        canActivate: [AuthGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(florasRoutes)],
  exports: [RouterModule],
  providers: [AuthGuard],
})
export class FlorasRoutingModule {}
