import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';

import {FlorasComponent} from './floras.component';
import {FloraStartComponent} from './flora-start/flora-start.component';
import {FloraListComponent} from './flora-list/flora-list.component';
import {FloraEditComponent} from './flora-edit/flora-edit.component';
import {FloraDetailComponent} from './flora-detail/flora-detail.component';
import {FloraItemComponent} from './flora-list/flora-item/flora-item.component';
import {FlorasRoutingModule} from './floras-routing.module';
import {SharedModule} from '../shared/shared.module';



@NgModule({
  declarations: [
    FlorasComponent,
    FloraStartComponent,
    FloraListComponent,
    FloraEditComponent,
    FloraDetailComponent,
    FloraItemComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlorasRoutingModule,
    SharedModule,
  ],
  providers: [],
})
export class FlorasModule {}
