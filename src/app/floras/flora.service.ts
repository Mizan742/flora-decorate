import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';

import {Flora} from './flora.model';
import {Ingredient} from '../shared/ingredient.model';
import {OrderListService} from '../order-list/order-list.service';

@Injectable()
export class FloraService {
  florasChanged = new Subject<Flora[]>();

  private floras: Flora[] = [
    new Flora(
      'Ceremonial Garland',
      'What a nice gift',
      'https://christmas.365greetings.com/wp-content/uploads/2017/10/Christmas-Tree-Garland-Decoration-Ideas-1.jpg',
      [new Ingredient('Flower', 5), new Ingredient('Leaves', 2)]
    ),
    new Flora(
      'Fall Garland',
      'Great for thanks giving',
      'https://encrypted-tbn3.gstatic.com/shopping?q=tbn:ANd9GcR4W2sMw113y5QdTulF5GgpUtgfK4VajyEvVK4NbK0KbXnOilPN3fj6-YAyVWBU7jht91gcYGUPv4X1f27wJEQYP_8dSAulvaNUOpe0fsk-&usqp=CAY',
      [new Ingredient('Flower', 1), new Ingredient('Leaves', 5)]
    ),

    new Flora(
      'Wedding Garland',
      '- Attractive',
      'https://i.etsystatic.com/11449796/r/il/355db6/1477384927/il_570xN.1477384927_nlud.jpg',
      [new Ingredient('Flower', 1), new Ingredient('Leaves', 0)]
    ),

    new Flora(
      'Wedding Car',
      '- Lurative',
      'https://4.imimg.com/data4/EW/DY/ANDROID-3318167/product-500x500.jpeg',
      [new Ingredient('Flower', 10), new Ingredient('Leaves', 5)]
    ),
  ];

  constructor(private slService: OrderListService) {}

  setFloras(floras: Flora[]) {
    this.floras = floras;
    this.florasChanged.next(this.floras.slice());
  }

  getFloras() {
    return this.floras.slice();
  }

  getFlora(index: number) {
    return this.floras[index];
  }

  addIngredientsToOrderList(ingredients: Ingredient[]) {
    this.slService.addIngredients(ingredients);
  }

  addFlora(flora: Flora) {
    this.floras.push(flora);
    this.florasChanged.next(this.floras.slice());
  }

  updateFlora(index: number, newFlora: Flora) {
    this.floras[index] = newFlora;
    this.florasChanged.next(this.floras.slice());
  }

  deleteFlora(index: number) {
    this.floras.splice(index, 1);
    this.florasChanged.next(this.floras.slice());
  }
}
