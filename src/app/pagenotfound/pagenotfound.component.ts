import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-pagenotfound',
  template: `<h2>Page Not Found.</h2>
              <div>
                <button (click)="goBack()">Go Back</button>
              </div>
            `

})
export class PagenotfoundComponent {
  constructor(private location: Location) { }
  goBack(): void {
    this.location.back();
  }
}
