import {Component, OnInit} from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  loadedFeature = 'flora';

  ngOnInit() {
    firebase.initializeApp({
      apiKey: 'AIzaSyCCvxvwbJXnhtTd77EMdqY4j9EIVi1T75I',
      authDomain: 'https://smart-d9daa.firebaseio.com',
    });
  }

  onNavigate(feature: string) {
    this.loadedFeature = feature;
  }
}
