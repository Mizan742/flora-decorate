import {NgModule} from '@angular/core';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';

import {OrderListComponent} from './order-list/order-list.component';
import {HomeComponent} from './core/home/home.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AngularBasicComponent} from './angular-basic/angular-basic.component';


const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {
    path: 'dashboard',
    component: DashboardComponent,
    loadChildren: './dashboard/dashboard.module#DashboardModule',
  },
  {path: 'floras', loadChildren: './floras/floras.module#FlorasModule'},
  {path: 'order-list', component: OrderListComponent},
  {
    path: 'basic',
    component: AngularBasicComponent,
    loadChildren: './angular-basic/angular-basic.module#AngularBasicModule',
  }
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {preloadingStrategy: PreloadAllModules}),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
