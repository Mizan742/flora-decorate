import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/Rx';

import {FloraService} from '../floras/flora.service';
import {Flora} from '../floras/flora.model';
import {AuthService} from '../auth/auth.service';

@Injectable()
export class DataStorageService {
  constructor(
    private http: Http,
    private floraService: FloraService,
    private authService: AuthService
  ) {}

  storeFloras() {
    const token = this.authService.getToken();
    return this.http.put(
      'https://smart-d9daa.firebaseio.com/floras.json?auth=' + token,
      this.floraService.getFloras()
    );
  }

  getFloras() {
    const token = this.authService.getToken();
    this.http
      .get('https://smart-d9daa.firebaseio.com/floras.json?auth=' + token)

      .map((response: Response) => {
        const floras: Flora[] = response.json();
        for (let flora of floras) {
          if (!flora['ingredients']) {
            flora['ingredients'] = [];
          }
        }
        return floras;
      })
      .subscribe((floras: Flora[]) => {
        this.floraService.setFloras(floras);
      });
  }
}
