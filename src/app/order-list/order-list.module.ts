import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {OrderListComponent} from './order-list.component';
// import {OrderEditComponent} from './order-edit/order-edit.component';

@NgModule({
 // declarations: [OrderListComponent, OrderEditComponent],
  declarations: [OrderListComponent],
  imports: [CommonModule, FormsModule],
})
export class OrderListModule {}
