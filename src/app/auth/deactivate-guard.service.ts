import { CanDeactivate} from '@angular/router';
import { Injectable } from '@angular/core';

import {FloraDetailComponent} from '../floras/flora-detail/flora-detail.component';

@Injectable()
export class DeactivateGuard implements CanDeactivate<FloraDetailComponent>{
canDeactivate(): boolean{
  return window.confirm("Are you sure to navigate ?");
}
}
