import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AngularBasicComponent} from './angular-basic.component';
import {PipeComponent} from './pipe/pipe.component';
import {ComponentTutorComponent} from './component-tutor/component-tutor.component';
import {DataBindingComponent} from './data-binding/data-binding.component';
import {EventBindingComponent} from './event-binding/event-binding.component';
import {TemplateComponent} from './template/template.component';
import {CustomDirectiveComponent} from './custom-directive/custom-directive/custom-directive.component';
import {RoutingComponent} from './routing/routing.component';
import {ServiceComponent} from './service/service.component';
import {HttpClientComponent} from './http-client/http-client.component';
import {FormsComponent} from './forms/forms.component';
import {VirtualScrollingComponent} from './virtual-scrolling/virtual-scrolling.component';
import {DragDropComponent} from './drag-drop/drag-drop.component';
import {AnimationComponent} from './animation/animation.component';
import {AngularMaterialComponent} from './angular-material/angular-material.component';
import {SpreadSheetComponent} from './spread-sheet/spread-sheet.component';
import {AngularLifecycleComponent} from './angular-lifecycle/angular-lifecycle.component';
import {NgRxComponent} from './ng-rx/ng-rx.component';
import {PagenotfoundComponent } from './../pagenotfound/pagenotfound.component';

import {ConfirmationGuard} from './../angular-basic/guard/confirmation.guard';
import {DeactivateGuard} from './../auth/deactivate-guard.service';
import {ObservableComponent} from './observable/observable.component';
import { Es6Component } from './es6/es6.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';


const dashboardRoutes: Routes = [
  {
    path: '',
     component: AngularBasicComponent,
    children: [
       {path: '', redirectTo: '/Component', pathMatch: 'full'},
       {path: 'Pipe', component: PipeComponent},
       {path: 'Component', component: ComponentTutorComponent},
       {path: 'Data_binding', component: DataBindingComponent},
       {path: 'Event_binding', component: EventBindingComponent},
       {path: 'Template', component: TemplateComponent},
       {path: 'Directive', component: CustomDirectiveComponent},
       {path: 'Routing', component: RoutingComponent},
       {path: 'Service', component: ServiceComponent},    
       {path: 'HTTPClient', component: HttpClientComponent, canDeactivate: [DeactivateGuard]},  
       {path: 'Form', component: FormsComponent, canDeactivate: [ConfirmationGuard]},
       {path: 'Virtual_Scrolling', component: VirtualScrollingComponent},
       {path: 'Drag_Drop', component: DragDropComponent},
       {path: 'Animation', component: AnimationComponent},
       {path: 'Angular_Material', component: AngularMaterialComponent},
       {path: 'Angular_LifeCycle', component: AngularLifecycleComponent},
       {path: 'Spread_sheet_XLS', component: SpreadSheetComponent},
       {path: 'Ng-Rx', component: NgRxComponent},
       {path: 'Observable', component: ObservableComponent},
       {path: 'Es6', component: Es6Component },
       {path: 'Reactive form', component: ReactiveFormComponent}
       /* {
        path: '**',
        component: PagenotfoundComponent    //this is working
      }	 */
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(dashboardRoutes)],
  exports: [RouterModule],
})
export class AngularBasicRouter {}

