import { Component, OnInit } from '@angular/core';
import { AngularService } from '../../angular-service';


@Component({
  selector: 'app-custom-directive',
  templateUrl: './custom-directive.component.html',
  // template: `
  //           <h1 class="titlePos">{{title}}</h1>
  //           <div appCustomDirective></div>
  //           `,
  styleUrls: ['./custom-directive.component.css'],
  styles: [`
  .text-success{
    color: green;

  }
  .text-danger{
    color: red;

  }
  .text-special{
    font-style: italic;
  }
  `
  ]
})
export class CustomDirectiveComponent implements OnInit {
  private title = "Angular custom directsive";
  private successfulText ="text-success";
  private userData = [];
  private hasError = true;
  private isSpecial =true;
  private state={
    "text-success" : !this.hasError,
    "text-danger" : this.hasError,
    "text-special" : this.isSpecial
  }


  constructor(private userDataService: AngularService) { }

  ngOnInit() {
    this.userDataService.getUserData().subscribe(response => {
      this.userData = response;
      console.log(response)
    });
  }

}
