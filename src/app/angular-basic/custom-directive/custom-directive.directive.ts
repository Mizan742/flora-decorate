import { Directive, ElementRef} from '@angular/core';

@Directive({
  selector: '[appCustomDirective]'
})
export class CustomDirectiveDirective {

  constructor(element : ElementRef) {  //ElementRef is mandatory
    element.nativeElement.innerText = "The text is coming from angular custom directive";
  }

}
