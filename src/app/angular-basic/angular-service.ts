import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';   //for http client
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class AngularService {
  private photoData;
  private urlPhotoData = "https://jsonplaceholder.typicode.com/photos";
  private urlUserData = "/assets/mock-data/users.json";
  private urlCardData = "/assets/mock-data/usersComp.json";


  constructor(private http: Http, private httpClient: HttpClient) { }
  getAngularTopics() {
    return this.http
      .get('/assets/mock-data/angularContent.json') // json files must be inside assets folder

      .map((response: Response) => {
        const data = response.json().data;

        return data;
      })
      .catch((error: Response) => {
        return Observable.throw('Something went wrong');
      });
  }

  getPhotoData(): Observable<[]> {
    return this.httpClient.get<[]>(this.urlPhotoData)
      .catch((error: Response) => {
        console.error(error);
        return Observable.throw(error);
      });
  }

  getUserData(): Observable<[]> {
    return this.httpClient.get<[]>(this.urlUserData)
      .catch((error: Response) => {
        console.error(error);
        return Observable.throw(error);
      });
  }

  getUserCardData(): Observable<[]> {
    return this.httpClient.get<[]>(this.urlCardData)
      .catch((error: Response) => {
        console.error(error);
        return Observable.throw(error);
      });
  }
}
