import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations'; //necessary for angular animation

@Component({
  selector: 'app-animation',
  templateUrl: './animation.component.html',
  styleUrls: ['./animation.component.css'],
  styles:[`
  div {
     margin: 0 auto;
     text-align: center;
     width:200px;
  }
  .rotate {
     width:100px;
     height:100px;
     border:solid 1px green;
  }
`],
animations: [
  trigger('myanimation',[
     state('smaller',style({
        transform : 'translateY(150px)'
     })),
     state('larger',style({
        transform : 'translateY(0px)'
     })),
     transition('smaller <=> larger', animate('500ms ease-in'))
  ])
]

})
export class AnimationComponent implements OnInit {
  title: string ="Angular animation";
  state: string = "smaller";

  constructor() { }

  ngOnInit() {
  }
  animate() {
    this.state= (this.state == 'larger') ? 'smaller' : 'larger';
  }

}
