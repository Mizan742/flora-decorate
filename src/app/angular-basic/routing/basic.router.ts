import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { HomeTestComponent } from '../routing/home-test/home-test.component';
import { ContactUsComponent } from '../routing/contact-us/contact-us.component'; 
import {RoutingComponent} from '../routing/routing.component';



const dashboardRoutes: Routes = [
  {
    path: '', component: RoutingComponent,
    children: [
       {path: 'home', component:HomeTestComponent}, 
       {path: 'contactus', component:ContactUsComponent} 
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(dashboardRoutes)],
  exports: [RouterModule],
})
export class BasicRouter {}

