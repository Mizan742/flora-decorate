import { Component, OnInit } from '@angular/core';
import { AngularService } from '../angular-service';

@Component({
  selector: 'app-header-basic',
  templateUrl: './header-basic.component.html',
  styleUrls: ['./header-basic.component.css']
})
export class HeaderBasicComponent implements OnInit {
  flags = [];
  angularSubjects = [];

  constructor(private angulartopic: AngularService) {   
    
  }

  ngOnInit() {
    
  }

  private angularSubject = this.angulartopic
    .getAngularTopics()
    .subscribe(
      (responseTopics) => { 
        responseTopics.forEach(element => {
          this.angularSubjects.push(element.title);
        });
      
      },
      error => console.log(error)
    );

  showContent(event) {
    console.log(event);
   
    }
  
}
