import { Component, OnInit, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.css']
})
export class CockpitComponent implements OnInit {
  @Output() serverCreated = new EventEmitter<{serverName: string, serverContent: string}>();
  @Output('bpCreated') blueprintCreated = new EventEmitter<{serverName: string, serverContent: string}>();
  // 'bpCreated' is the alias name. If i pass alias name then in the parent html file it will be included
  // If I don't use alias name then no param is necessary to emit

  @ViewChild('serverContentInput', { static: false }) serverContentInput: ElementRef;
  // serverContentInput is the template reference
  // ViewChild is used here to introduce  the template reference serverContentInput inside the functions.
  constructor() { }

  ngOnInit() {
  }

  onAddServer(nameInput: HTMLInputElement) {    //first template reference
    this.serverCreated.emit({
      serverName: nameInput.value,
      serverContent: this.serverContentInput.nativeElement.value   // second template reference
    });
  }

  onAddBlueprint(nameInput: HTMLInputElement) {
    this.blueprintCreated.emit({
      serverName: nameInput.value,
      serverContent: this.serverContentInput.nativeElement.value
    });
  }

}
