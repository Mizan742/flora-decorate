import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';   //necessary for reactive/model driven form
import {CanComponentDeactivate} from '../../angular-basic/guard/confirmation.guard';
// import { getMaxListeners } from 'cluster';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css']
})
export class FormsComponent implements OnInit, CanComponentDeactivate {
  private title1 = "Template Driven Form";
  private title2 = "Model Driven/reactive Form";
  private title3 = "Model driven form + custom validation";
  private formdata;    //necessary for reactive/model driven form
  private formDataValidation;   //necessary for reactive/model driven form validation
  private email = "";
  private emailValid = "";

  constructor() { }

  ngOnInit() {
    this.formdata = new FormGroup({           //necessary for reactive/model driven form
      email: new FormControl("mizanrm742@gmail.com"),
      password: new FormControl("MMRah321")
    });

    this.formDataValidation = new FormGroup({   //necessary for reactive/model driven form validation
      emailID: new FormControl("", Validators.compose([
        Validators.required,
        Validators.pattern("[^@]*@[^@]*")
      ])),
      /*password: new FormControl("", [
        Validators.required,
        Validators.minLength(8)   //password min length must be 8+ characters
      ])*/
      password: new FormControl("", [this.passwordvalidation])   //necessary for reactive/model driven form validation
    });
  }
  passwordvalidation(formcontrol) {
    if (formcontrol.value.length < 5) {
       return {"password" : true};
    }
 }

  onClickSubmit(data) {
    alert("Entered" + "\n" + " Email id : " + data.Email + "\n" + "Password : " + data.passwd);
  }

  modelDrivenForm(formdata) {
    this.email = formdata.email;
    alert("Entered" + "\n" + " Email id : " + formdata.email + "\n" + "Password : " + formdata.password);
  }

  modelDrivenFormValidation(formdata) {
    this.emailValid = formdata.emailID;
    alert("Entered" + "\n" + " Email id : " + formdata.emailID + "\n" + "Password : " + formdata.password);
  }

  confirm(){
    return confirm('Are you sure want to navigate away?');
  }


}
