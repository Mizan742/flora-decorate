import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.css']
})
export class DataBindingComponent implements OnInit {
  title ="Angular data binding";
  months = ["January", "February", "March", "April", "May", "June", "July","August", "September", "October", "November", "December"];
  condition1= false;
  //condition1= true;
  condition2=  true;
  name="My name Mizan";

  constructor() { }

  ngOnInit() {
  }

}
