import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {
  isAvailable: boolean = true;
  title = "ng-template";
  constructor() { }

  ngOnInit() {
  }

  changeFlagStatus(){
    this.isAvailable =!this.isAvailable
  }

}
