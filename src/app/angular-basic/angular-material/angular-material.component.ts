import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-angular-material',
  templateUrl: './angular-material.component.html',
  styleUrls: ['./angular-material.component.css']
})
export class AngularMaterialComponent implements OnInit {
  title = "Angular Material";

  constructor() { }

  ngOnInit() {
  }

}
