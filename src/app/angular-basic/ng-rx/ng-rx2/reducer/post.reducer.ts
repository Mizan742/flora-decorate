import * as PostActions from '../../../ng-rx/ng-rx2/action/post.action';
import { Post } from '../../../ng-rx/ng-rx2/model/post.model';

export type Action = PostActions.All;

//default app state
const defaultState: Post = {
    text: 'Hello, I am the default post',
    likes: 0
}

//Helper function to create new state object
const newState = (state, newData) => {
    return Object.assign({}, state, newData)
}

//reducer function
export function postReducer(state: Post = defaultState, action: Action) {
    console.log(action.type, state);
    switch (action.type) {
        case PostActions.EDIT_TEXT:
            return newState(state, {text: action.payload});
        case PostActions.UPVOTE:
            return newState(state, {likes:state.likes+1});
        case PostActions.DOWNVOTE:
            return newState(state, {likes: state.likes-1});
        case PostActions.RESET:
            return defaultState;
        default:
            return defaultState;

    }


}


