import { Component, OnInit } from '@angular/core';
import { Store,select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Post } from './model/post.model';
import * as PostActions from './action/post.action'

interface AppState {
  post: Post;
}

@Component({
  selector: 'app-ng-rx2',
  templateUrl: './ng-rx2.component.html'
})
export class NgRx2Component implements OnInit {

  post: Observable<Post>


  textInput: string =""; // form input value

  constructor(private store: Store<AppState>) {
    this.post = store.pipe(select('post'));
    console.log(this.post);
  }

  ngOnInit(){
    this.resetPost()
  }

  editText() {
    this.store.dispatch(new PostActions.EditText(this.textInput) )
  }

  resetPost() {
    this.store.dispatch(new PostActions.Reset())
  }

  upvote() {
    this.store.dispatch(new PostActions.Upvote())
  }

  downvote() {
    this.store.dispatch(new PostActions.Downvote())
  }

}
