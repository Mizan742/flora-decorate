import { Blockchain } from './blockchain.model';

export interface AppState {
  readonly blockchain: Blockchain[];
}