import { Component, OnInit } from '@angular/core';
import { Store} from '@ngrx/store';
import { Observable } from 'rxjs';
interface AppState {
  message: string;
}

@Component({
  selector: 'app-ng-rx0',
  templateUrl: './ng-rx0.component.html'
})
export class NgRx0Component{
  message$: Observable<string>
  constructor(private store: Store<AppState>){
    this.message$ = this.store.select('message')
  }

  spanishMessage(){
    this.store.dispatch({type: 'SPANISH'});
  }
  frenchMessage(){
    this.store.dispatch({type: 'FRENCH'});
  }
}
