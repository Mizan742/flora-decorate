import { Component, OnInit } from '@angular/core';
import * as alasql from 'alasql';



@Component({
  selector: 'app-spread-sheet',
  templateUrl: './spread-sheet.component.html',
  styleUrls: ['./spread-sheet.component.css']
})
export class SpreadSheetComponent implements OnInit {
  title = "Render XLS file at browser";
  xlsData =[];
  sum = "";

  constructor() { }

  ngOnInit() {

  }
  loadFile(event) {
    alasql["private"].externalXlsxLib = require('xlsx');
    var self=this;
    alasql('SELECT * FROM FILE(?,{headers:true})',[event],function(data){
      self.xlsData = data;
      self.sum+= JSON.stringify(data, null, '\t') + "\n";  //convert to json
      console.log(self.sum);
    });
  }
}


//npm install --save alasql
//npm install xlsx@0.12.12 or npm install xlsx@latest