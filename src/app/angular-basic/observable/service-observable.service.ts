import { Injectable } from '@angular/core';
import { mergeMap } from 'rxjs/operators';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { of } from 'rxjs/observable/of';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';   //for http client


@Injectable({
  providedIn: 'root'
})
export class ServiceObservableService {

  constructor(private http: Http, private httpClient: HttpClient) { }

  obaservableAll() {
    const myPromise = val =>
      new Promise(resolve =>
        setTimeout(() => resolve(`Promise Resolve:${val}`), 1000)
      );

    const source = of([1, 2, 3, 4, 5]);   //emit array of all 5 results
    const example = source.pipe(mergeMap(q => forkJoin(...q.map(myPromise))));
    return example;
  }

  observerAllChartData() {
    let observable1 = this.observableFunc('/assets/mock-data/health.json');
    let observable2 = this.observableFunc('/assets/mock-data/litre.json');
    let observable3 = this.observableFunc('/assets/mock-data/heatMap.json');
    let observable4 = this.observableFunc('/assets/mock-data/empLarge.json');

    const combindObservable = forkJoin([observable1,observable2,observable3,observable4]);
    return combindObservable; 
  }
  observableFunc(url: string): Observable <[]> {
      return this.httpClient.get<[]>(url)
      .catch((error: Response) => {
        return Observable.throw('Something went wrong');
      });
  }

  observableRetry(): Observable <[]> {
    return this.httpClient.get<[]>('/assets/mock-data/heatMap.jsons')
    .catch((error: Response) => {
      return Observable.throw('Something went wrong');
    });
  }
}
