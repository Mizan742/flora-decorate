import { Component, OnInit } from '@angular/core';
import { ServiceObservableService } from '../../angular-basic/observable/service-observable.service';
import { ISubscription } from 'rxjs/subscription'

import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';

// import { Observable } from 'rxjs';


@Component({
  selector: 'app-observable',
  templateUrl: './observable.component.html',
  styleUrls: ['./observable.component.css']
})
export class ObservableComponent implements OnInit {
  private data: string[];
  private combindChartData: any;
  private title: string = "Observable";
  private responseRetry: any;
  private statusmessage: string;
  private subscription: ISubscription;

  constructor(private serviceData: ServiceObservableService) { }

  ngOnInit() {
    this.serviceData.obaservableAll()
      .subscribe(val => this.data = val)

    this.serviceData.observerAllChartData()
      .subscribe(combindObservable => {
        this.combindChartData = combindObservable;
        console.log(this.combindChartData)
      });

    this.subscription = this.serviceData.observableRetry()
      //.retry(3)// it will try 3 times to fetch the data from backend
      //.retryWhen((err)=>err.delay(1000)) //param is the error observable and will retry every 1 second infinite amount time
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 20) {
            this.statusmessage = 'Retrying … Attempt #' + retryCount;
            return retryCount;
          }
          else {
            throw (err)
          }     
          }, 0).delay(1000)
      })
      .subscribe(response =>{
      this.responseRetry = response;
})    
}

onBackbuttonClick(){
  this.statusmessage = "Request cancelled";
  this.subscription.unsubscribe();
}

}

  

