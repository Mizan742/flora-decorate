import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-component-tutor',
  templateUrl: './component-tutor.component.html',
  styleUrls: ['./component-tutor.component.css']
})
export class ComponentTutorComponent implements OnInit {
  title = "New component is created";
  title1 = "Component Communication";
  compTitle = "Angular component";

  constructor() { }

  ngOnInit() {
  }

}
