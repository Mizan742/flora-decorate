import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-to-sibling',
  template: `
  <div class="row">
    <div class="col-sm-7"> <app-parent-sibling></app-parent-sibling></div>
    <div class="col-sm-5"><app-sibling></app-sibling></div>
  </div>
  `,
  styleUrls: ['./to-sibling.component.css']
})
export class ToSiblingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
