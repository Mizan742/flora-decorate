import { Component, OnInit} from '@angular/core';
import {DataService} from '../to-sibling/data.service';

@Component({
  selector: 'app-child-sibling',
  template:`
   <div class="child">
  <h3>{{title}}</h3>
  {{message}}
  </div>
  `,
  styleUrls: ['./to-sibling.component.css']
})
export class ChildSiblingComponent implements OnInit {
  message: string;                    
 
  title="Child";
  constructor(private data: DataService) { }

  ngOnInit() {
    this.data.currentMessage.subscribe(message => this.message =message)

  }
}
