import { Component, OnInit} from '@angular/core';
import { DataService } from '../to-sibling/data.service';

@Component({
    selector: 'app-parent-sibling',
    template: `
    <div class="parent">
        <h2>{{title}}</h2>
        {{message}}

    <app-child-sibling></app-child-sibling>    
    
    </div>
     `,
    styleUrls: ['./to-sibling.component.css']
})
export class ParentSiblingComponent implements OnInit {  //after view is necessary for viewchild
    title = "Parent";
    message: string;

    constructor(private data: DataService) { }

    ngOnInit() {
        this.data.currentMessage.subscribe(message => this.message = message)
    }

}
