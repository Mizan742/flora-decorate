import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';

@Component({
  selector: 'app-sibling',
  template: `
   <div class="child">
  <h3>{{title}}</h3>
  {{message}}

  <button class="btn btn-primary" (click)="changeMessageObs()">Send New Message</button>
  </div>
  `,
  styleUrls: ['./to-sibling.component.css']
})
export class SiblingComponent implements OnInit {
  message: string;

  title = "Sibling";
  constructor(private data: DataService) { }

  ngOnInit() {
    this.data.currentMessage.subscribe(message => this.message = message)
  }
  changeMessageObs(){
    this.data.changeMessage("Hello message from Sibling");
  }
}
