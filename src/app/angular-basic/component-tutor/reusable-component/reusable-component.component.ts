import { Component, OnInit } from '@angular/core';
import {AngularService} from '../../angular-service';

@Component({
  selector: 'app-reusable-component',
  templateUrl: './reusable-component.component.html',
  styleUrls: ['./reusable-component.component.css']
})
export class ReusableComponentComponent implements OnInit {
  private userData: any;

  constructor(private userDataService : AngularService) {}

  ngOnInit() {
    this.userDataService.getUserCardData().subscribe(response => {
      this.userData = Array.from(Object.keys(response), k=>response[k])[0];
      console.log(response)
    });
  }
  

}
