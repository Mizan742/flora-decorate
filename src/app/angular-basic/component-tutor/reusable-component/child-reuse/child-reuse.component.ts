import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-child-reuse',
  templateUrl: './child-reuse.component.html',
  styleUrls: ['./child-reuse.component.css']
})
export class ChildReuseComponent implements OnInit {
  @Input() user: any;  

  constructor() { }

  ngOnInit() {
  }

}
