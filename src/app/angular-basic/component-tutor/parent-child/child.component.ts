import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-child',
  template:`
   <div class="child">
  <h3>{{title}}</h3>
  <strong>From Parent  -> {{message}}</strong>
  </div>
  `,
  styleUrls: ['./parent-child.component.css']
})
export class ChildComponent implements OnInit {
  @Input() message: string;                     // necesary for parent ->child communication
  title="Child";
  constructor() { }

  ngOnInit() {
  }

}
