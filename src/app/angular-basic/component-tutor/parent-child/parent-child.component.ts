import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent-child',
  template: `<app-parent></app-parent>`,
  styleUrls: ['./parent-child.component.css']
})
export class ParentChildComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
