import { Component, OnInit } from '@angular/core';
//message passing from parent
@Component({
    selector: 'app-parent',
    template: `
    <div class="parent">
    <h2>{{title}}</h2>
     <app-child [message]="msg"></app-child>         
    </div>
     `,
    styleUrls: ['./parent-child.component.css']
})
export class ParentComponent implements OnInit {
    title = "Parent";
    msg = "Hello child";
    constructor() { }

    ngOnInit() {
    }

}
