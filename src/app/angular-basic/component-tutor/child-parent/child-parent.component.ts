import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-child-parent',
  template: `<app-parent1></app-parent1>`,
  styleUrls: ['./child-parent.component.css']
})
export class ChildParentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
