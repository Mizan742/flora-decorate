import { Component, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-child1',
  template:`
   <div class="child">
  <h3>{{title}}</h3>
  <button (click)="sendMessage()">Send Msg</button>
  </div>
  `,
  styleUrls: ['./child-parent.component.css']
})
export class Child1Component implements OnInit {
  message: string = "From child  -> Hello parent";                    
  @Output() messageEvent = new EventEmitter<string>();   // necesary for child -> parent communication
  title="Child";
  constructor() { }

  ngOnInit() {
  }

  sendMessage(){
    this.messageEvent.emit(this.message);    // necesary for child -> parent communication
  }

}
