import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-parent1',
    template: `
    <div class="parent">
    <h2>{{title}}</h2>
    <strong>{{msg}}</strong>
    <app-child1 (messageEvent)="receiveMessage($event)"></app-child1>    
    </div>
     `,
    styleUrls: ['./child-parent.component.css']
})
export class Parent1Component implements OnInit {
    title = "Parent";
    msg = "";
    constructor() { }

    ngOnInit() {
    }

    receiveMessage($event){ 
        this.msg = $event;
    }

}
