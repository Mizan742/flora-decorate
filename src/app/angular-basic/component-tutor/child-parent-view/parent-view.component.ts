import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ChildViewComponent } from '../child-parent-view/child-view.component';

@Component({
    selector: 'app-parent-view',
    template: `
    <div class="parent">
        <h2>{{title}}</h2>
        <p>{{msg}}</p>

    <app-child-view></app-child-view>    
    
    </div>
     `,
    styleUrls: ['./child-parent-view.component.css']
})
export class ParentViewComponent implements AfterViewInit {  //after view is necessary for viewchild
    title = "Parent";
    msg =" ";

    @ViewChild(ChildViewComponent, { static: true })
    child: ChildViewComponent;

    constructor() { }

    ngOnInit() {
    }
    ngAfterViewInit() {
        this.msg = this.child.message;
        console.log(this.msg);
    }

}
