import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-child-parent-view',
  template: `<app-parent-view></app-parent-view>`,
  styleUrls: ['./child-parent-view.component.css']
})
export class ChildParentViewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
