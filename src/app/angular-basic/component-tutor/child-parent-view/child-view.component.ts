import { Component, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-child-view',
  template:`
   <div class="child">
  <h3>{{title}}</h3>
  </div>
  `,
  styleUrls: ['./child-parent-view.component.css']
})
export class ChildViewComponent implements OnInit {
  message: string = "From child  -> Hello parent";                    
 
  title="Child";
  constructor() { }

  ngOnInit() {
  }
}
