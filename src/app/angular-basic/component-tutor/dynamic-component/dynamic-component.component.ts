import { Component, OnInit } from '@angular/core';
import {DynamicComponent1Component} from '../../component-tutor/dynamic-component/dynamic-component1/dynamic-component1.component';
import {DynamicComponent2Component} from '../../component-tutor/dynamic-component/dynamic-component2/dynamic-component2.component';
import {DynamicComponent3Component} from '../../component-tutor/dynamic-component/dynamic-component3/dynamic-component3.component';


@Component({
  selector: 'app-dynamic-component',
  templateUrl: './dynamic-component.component.html',
  styles:[`
  .space {
    margin: 0 8px;
  } 
` ]

})
export class DynamicComponentComponent implements OnInit {
  private dynamicComp: any;

  constructor() { }

  ngOnInit() {
  }

  loadComponent(event){
    this.dynamicComp = event.target.textContent;
    if(event.target.textContent === 'Dynamic component 1'){
      this.dynamicComp = DynamicComponent1Component;
    }
    else if(event.target.textContent === 'Dynamic component 2'){
      this.dynamicComp = DynamicComponent2Component;
    }
    else{
      this.dynamicComp = DynamicComponent3Component;
    }

  }

}
