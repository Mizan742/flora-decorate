import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComponentTutorComponent } from '../component-tutor/component-tutor.component';
import { ParentChildComponent } from '../component-tutor/parent-child/parent-child.component';
import { ChildParentComponent } from '../component-tutor/child-parent/child-parent.component';
import { ToSiblingComponent } from '../component-tutor/to-sibling/to-sibling.component';
import { ChildParentViewComponent } from '../component-tutor/child-parent-view/child-parent-view.component';
import { ReusableComponentComponent } from '../component-tutor/reusable-component/reusable-component.component';
import { DynamicComponentComponent } from '../component-tutor/dynamic-component/dynamic-component.component'



const dashboardRoutes: Routes = [
  {
    path: '', component: ComponentTutorComponent,
    children: [
      { path: 'parentToChild', component: ParentChildComponent },
      { path: 'childToParent', component: ChildParentComponent },
      { path: 'childToParentView', component: ChildParentViewComponent },
      { path: 'toSibling', component: ToSiblingComponent },
      { path: 'reuseComponent', component: ReusableComponentComponent },
      { path: 'dynamicComponent', component: DynamicComponentComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(dashboardRoutes)],
  exports: [RouterModule],
})
export class ComponentCommunicationRouter { }

