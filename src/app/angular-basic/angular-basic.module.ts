import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';              //necessary for httpClient
import { BasicRouter } from './routing/basic.router';
import { AngularBasicRouter } from './angular-basic.router';
import { ComponentCommunicationRouter } from './component-tutor/component-communication.router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';       //necessary for virtual scrolling
import { DragDropModule } from '@angular/cdk/drag-drop';               //necessary for Materials/CDK-Drag and Drop
import { MatButtonModule, MatMenuModule, MatSidenavModule,  MatDatepickerModule, MatInputModule, MatNativeDateModule} from '@angular/material';  //necessafry for angular material

import { AngularBasicComponent } from './angular-basic.component';
import { HeaderBasicComponent } from './header-basic/header-basic.component';
import { PipeComponent } from './pipe/pipe.component';
import { ComponentTutorComponent } from './component-tutor/component-tutor.component';
import { DataBindingComponent } from './data-binding/data-binding.component';
import { EventBindingComponent } from './event-binding/event-binding.component';
import { TemplateComponent } from './template/template.component';
import { CustomDirectiveDirective } from './custom-directive/custom-directive.directive';
import { CustomDirectiveComponent } from './custom-directive/custom-directive/custom-directive.component';
import { SqrtFilterPipe } from './pipe/custom-filter/sqrt-filter.pipe';
import { RoutingComponent } from './routing/routing.component';
import { HomeTestComponent } from './routing/home-test/home-test.component';
import { ContactUsComponent } from './routing/contact-us/contact-us.component';
import { HeaderComponent } from './routing/header/header.component';
import { ServiceComponent } from './service/service.component';
import { HttpClientComponent } from './http-client/http-client.component';
import { FormsComponent } from './forms/forms.component';
import { SpreadSheetComponent } from './spread-sheet/spread-sheet.component';
import { ParentChildComponent } from './component-tutor/parent-child/parent-child.component';
import { ChildParentComponent } from './component-tutor/child-parent/child-parent.component';
import { ToSiblingComponent } from './component-tutor/to-sibling/to-sibling.component';
import { ChildComponent } from './component-tutor/parent-child/child.component';
import { ParentComponent } from './component-tutor/parent-child/parent.component';
import { Child1Component } from './component-tutor/child-parent/child1.component';
import { Parent1Component } from './component-tutor/child-parent/parent1.component';
import { ChildParentViewComponent } from './component-tutor/child-parent-view/child-parent-view.component';
import { ChildViewComponent } from './component-tutor/child-parent-view/child-view.component';
import { ParentViewComponent } from './component-tutor/child-parent-view/parent-view.component';
import { ChildSiblingComponent } from './component-tutor/to-sibling/child-sibling.component';
import { ParentSiblingComponent } from './component-tutor/to-sibling/parent-sibling.component';
import { SiblingComponent } from './component-tutor/to-sibling/sibling.component';
import { DataService } from './component-tutor/to-sibling/data.service';
import { VirtualScrollingComponent } from './virtual-scrolling/virtual-scrolling.component';
import { DragDropComponent } from './drag-drop/drag-drop.component';
import { AnimationComponent } from './animation/animation.component';
import { AngularMaterialComponent } from './angular-material/angular-material.component';
import { NgcontentComponent } from './custom-directive/custom-directive/ngcontent/ngcontent.component';
import { ReusableComponentComponent } from './component-tutor/reusable-component/reusable-component.component';
import { ChildReuseComponent } from './component-tutor/reusable-component/child-reuse/child-reuse.component';
import { DynamicComponentComponent } from './component-tutor/dynamic-component/dynamic-component.component';
import { DynamicComponent1Component } from './component-tutor/dynamic-component/dynamic-component1/dynamic-component1.component';
import { DynamicComponent2Component } from './component-tutor/dynamic-component/dynamic-component2/dynamic-component2.component';
import { DynamicComponent3Component } from './component-tutor/dynamic-component/dynamic-component3/dynamic-component3.component';
import { AngularLifecycleComponent } from './angular-lifecycle/angular-lifecycle.component';
import { CockpitComponent } from './angular-lifecycle/cockpit/cockpit.component';
import { ServerElementComponent } from './angular-lifecycle/server-element/server-element.component';
import { NgRxComponent } from './ng-rx/ng-rx.component';
import { NgRx1Component } from './ng-rx/ng-rx1/ng-rx1.component';
import { NgRx2Component } from './ng-rx/ng-rx2/ng-rx2.component';
import { NgRx0Component } from './ng-rx/ng-rx0/ng-rx0.component';
import { NgRx3Component } from './ng-rx/ng-rx3/ng-rx3.component';
import { DisplayComponent } from './ng-rx/ng-rx3/display/display.component';
import { PagenotfoundComponent } from './../pagenotfound/pagenotfound.component';

import {ConfirmationGuard} from './../angular-basic/guard/confirmation.guard';
import {DeactivateGuard} from './../auth/deactivate-guard.service';
import { ObservableComponent } from './observable/observable.component';
import { Es6Component } from './es6/es6.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import { ProfileEditorComponent } from './reactive-form/profile-editor/profile-editor.component';
import { NameEditorComponent } from './reactive-form/name-editor/name-editor.component';


@NgModule({
  declarations: [AngularBasicComponent, HeaderBasicComponent, PipeComponent, ComponentTutorComponent,
    DataBindingComponent, EventBindingComponent, TemplateComponent, CustomDirectiveDirective,
    CustomDirectiveComponent, SqrtFilterPipe, RoutingComponent, HomeTestComponent,
    ContactUsComponent, HeaderComponent, ServiceComponent, HttpClientComponent,
    FormsComponent, SpreadSheetComponent, ParentChildComponent, ChildParentComponent,
    ToSiblingComponent, ChildComponent, ParentComponent, Child1Component,
    Parent1Component, ChildParentViewComponent, ChildViewComponent, ParentViewComponent,
    ChildSiblingComponent, ParentSiblingComponent, SiblingComponent, VirtualScrollingComponent, DragDropComponent, AnimationComponent, 
    AngularMaterialComponent, NgcontentComponent, ReusableComponentComponent, ChildReuseComponent, 
    DynamicComponentComponent, DynamicComponent1Component, DynamicComponent2Component, 
    DynamicComponent3Component, AngularLifecycleComponent, CockpitComponent, ServerElementComponent, 
    NgRxComponent, NgRx1Component, NgRx2Component, NgRx0Component, NgRx3Component, DisplayComponent, PagenotfoundComponent, ObservableComponent, Es6Component, ReactiveFormComponent, ProfileEditorComponent, NameEditorComponent], //component/pipe/directive names
    entryComponents: [        //necessary for dynamic components
      DynamicComponent1Component,
      DynamicComponent2Component,
      DynamicComponent3Component
    ],

  imports: [
    CommonModule,
    AngularBasicRouter,
    BasicRouter,
    ComponentCommunicationRouter,
    FormsModule,
    ReactiveFormsModule,    //necessary for model driven form 
    HttpClientModule,     //necessary for httpClient
    ScrollDispatchModule,   //necessary for virtual scrolling
    DragDropModule,        //necessary for Materials/CDK-Drag and Drop
    MatButtonModule, 
    MatMenuModule, MatSidenavModule, MatDatepickerModule, MatInputModule, 
    MatNativeDateModule,   //necessary for angular material
  ],
  providers: [DataService,
              ConfirmationGuard,
              DeactivateGuard],
})
export class AngularBasicModule { }
  
