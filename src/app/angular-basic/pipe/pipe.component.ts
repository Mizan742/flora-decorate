import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable'; 


@Component({
  selector: 'app-pipe',
  templateUrl: './pipe.component.html',
  styleUrls: ['./pipe.component.css']
})
export class PipeComponent implements OnInit {
  compTitle = "Pipe | filter test";
  todaysDate = new Date();
  observableData: Observable<number>;
  promisedata;

  person ={
    name:'Mizan',
    age:30,
    income: 64567,
    fraction: 454.7878814,
    address:{
             address1 : 'USA',
             address2 : 'Bangladesh'
    },
    percent: 0.12
  };
  months = ["Jan", "Feb", "Mar", "April", "May", "Jun", "July", "Aug", 
  "Sept", "Oct", "Nov", "Dec"]; 

  constructor() { 
    this.observableData = this.getObservable();
    this.promisedata = this.getPromise();
  }

  ngOnInit() {
  }

  getObservable(){    //for async pipe with observable
    return Observable
    .interval(1000)
    .take(10)        //take number 1,2,3,4,5,6,7,8,9 and return square
    .map((v) => v*v)
  }

  getPromise() {     //for async pipe with promise
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve("Promise complete!"), 3000);
    });
 }

}
