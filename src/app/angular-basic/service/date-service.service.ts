import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DateServiceService {
  date= {};
  serviceProperty = "New service is created";

  constructor() { }
  todayDate(){
     return this.date= new Date();
  }
}
