import { Component, OnInit } from '@angular/core';
import {DateServiceService} from  './date-service.service'

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.css']
})
export class ServiceComponent implements OnInit {
  title ="Angular service";
  todaysDate;
  serviceContent: string;
  constructor(private myService : DateServiceService) {
    
  }

  ngOnInit() {
    this.todaysDate = this.myService.todayDate(); 
  }
  changeServiceProperty(){
    this.myService.serviceProperty = "The service property content has changed";
  }

}
