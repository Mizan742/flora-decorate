import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-event-binding',
  templateUrl: './event-binding.component.html',
  styleUrls: ['./event-binding.component.css']
})
export class EventBindingComponent implements OnInit {
  title ="Angular event binding";
  months = ["January", "February", "March", "April", "May", "June", "July","August", "September", "October", "November", "December"];

  constructor() { }

  ngOnInit() {
  }

  eventBinding(event){
    alert(event.target.outerText+" was clicked");
    console.log(event);
  }
  changeMonth(event){
    alert(event.target.value+" was selected");
    console.log(event);

  }


}
