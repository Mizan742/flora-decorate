import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-es6',
  templateUrl: './es6.component.html',
  styleUrls: ['./es6.component.css']
})
export class Es6Component implements OnInit {
  private title = "ES6 Features";
  private contents = ["Strict mode", "Hoisting","let", "const", "ternary operator", "spread operator(addition with reduce)",
  "spread operator(array)","spread operator(object)","spread operator (function param)","function","anonymous function", "anonymous function parameterize",
  "function constractor", "function recursion", "anonymous recursion function /  self executive function",
  "Lamda/arrow function (single line)", "Lamda/arrow function (multi lines)", "function hoisting",
  "Immediately Invoked Function Expression (IIFE)", "Generator Function", "Generator Function(param passing)","arrow function (This)",
  "arrow function (map)", "arrow function (filter)"];

  constructor() { }

  ngOnInit() {
  }

  evaluateFeature(index : number){
    if(index === 0){
      this.strictMode();
      return 'Strict mode is verified with keyword "use strict"';
    } else if (index === 1){
      this.hoisting();
      return 'Without Strict mode variable/function hoisting works'
    } else if (index === 2){
      this.letkeytest();
      return 'let was tested';
    } else if (index === 3){
      this.constkeytest();
      return 'const is block scoped immutable but mutable for object';
    } else if (index === 4){  
      return this.ternaryOP();
    } else if (index === 5){     
      return this.spreadOp();
    } else if (index === 6){     
      return this.spreadOpArray();
    } else if (index === 7){     
      return this.spreadOpObject();
    } else if (index === 8){     
      return this.spreadOpfunction();
    } else if (index === 9){     
      return this.functions();
    } else if (index === 10){     
      return this.functionAnonymous();
    } else if (index === 11){     
      return this.functionAnonymousParam();
    } else if (index === 12){     
      return this.functionConstractor();
    } else if (index === 13){     
      return this.functionRecursion();
    } else if (index === 14){     
      return this.functionSelfExecutive();
    } else if (index === 15){     
      return this.arrowFunctionSingleLine();
    } else if (index === 16){     
      return this.arrowFunctionMultiLine();
    } else if (index === 17){     
      return this.functionHoist();
    } else if (index === 18){     
      return this.iife();
    } else if (index === 19){     
      return this.generatorFunction();
    } else if (index === 20){     
      return this.generatorFunctionParam();
    } else if (index === 21){     
      return this.arrowFunctionThis();
    } else if (index === 22){     
      return this.arrowFunctionMap();
    } else if (index === 23){     
      return this.arrowFunctionFilter();
    }
  }

  strictMode(): void {
    "use strict"; 
     const v = "Hi!  I'm a strict mode script!"; 
  }

  hoisting(): void {
    v = "Hi!  I'm a strict mode script!"; 
    var v = "hello"
    //console.log(v);
  }

  letkeytest(): void {
    let count = 100;        
    // let count = 1000;     //in the same block we cannot redeclare let variable
    for (let count = 1;count <= 10;count++) {    //inside for loop brackets ,count value starts from 1
       //console.log("count value inside loop is ",count);
    }
    //console.log("count value after loop is",count);  //outside for loop brackets ,count value is 100
   
 
    if(count == 100){
       let count = 50;                               //inside if brackets ,count value is 50
       //console.log("count inside if block",count);
    }
    //console.log(count);      //outer most ,count value is 100  
  }

  constkeytest(): void {
    const abc =12;
    //abc =15;   //const are immutable block scoped 

    const abcd = {   //const are mutable in the case of object;
      name: "mizan",
      age:30,
      class: 6
    }
    abcd.class = 7;      //accepted with const
    //console.log(abcd.class);
  }

  ternaryOP(): string {
    var num = -2 
    return num > 0 ? "positive" : "negative";
  }

  addNumbersReduce(...x: number[]){    // here x is the array elements
    return x.reduce((a, b) => a + b, 0);
 }

  spreadOp(): string {
    const arr: number[] = [10,20,30,40,50];
    const sample1=this.addNumbersReduce(...arr);
    const sample2=this.addNumbersReduce(...[1,2,3,4,5,6,7,8,9,10]);
    return 'The sum is '+(sample1+sample2);
  }
 

  
  spreadOpArray(): string {
    const arr: number[] = [10,20,30,40,50];
    //console.log(...arr);                      //output 10 20 30 40 50
    const arr1: number[] = [1,2,3,4,5];
    //console.log(...arr1);                      //output 1,2,3,4,5
    const arr2: number[] = [-1,-2,-3,-4,-5];
    //console.log(...arr2);                      //output -1,-2,-3,-4,-5

    return 'concatenation of ' + arr + ' ,      ' + arr1 + '     and    '+arr2 + '   is ----   '+[...arr, ...arr1, ...arr2];
  }

  spreadOpObject(): string {
    let student1 ={                               
      firstName:'Mohtashim',
      company:'TutorialsPoint'
    };   
    let student2 ={...student1}                      //copy object
    //console.log(student2)                            
    /*output
    {                               
      firstName:'Mohtashim',
      company:'TutorialsPoint'
    };   
    */

    //concatenate objects
    let student3 = {lastName:'Mohammad'}
    let student4 = {...student2,...student3}
    // console.log(student4)
  return 'concatenation of'+JSON.stringify(student2)+ ' and '+JSON.stringify(student3)+ ' is-----     '+JSON.stringify(student4);
}
addTwoNumbers(first,second = 10){
  const firstval ='first parameter is :'+first;
  const secontval ='second parameter is :'+second;
  return firstval+'<br>'+secontval+'<br>'+(first+second);
}

functions(): string {
  let sum: string='';
  sum+="case 1 :"+this.addTwoNumbers(20)+"<br>";
  sum+="case 2 :"+this.addTwoNumbers(2,3)+"<br>";
  //sum+="case 3 :"+this.addTwoNumbers()+"<br>";
  sum+="case 4 "+this.addTwoNumbers(1,null)+"<br>";
  sum+="case 5 "+this.addTwoNumbers(3,undefined);

  return sum;
}

funclength(...params: any[]) { 
  return params+' has length '+params.length +'<br>'; 
}  

spreadOpfunction(): string {
  let sum ='';  
  const arr=[1,2,3,4,5,6,7];
  sum+=this.funclength();  
  sum+=this.funclength(5); 
  sum+=this.funclength(5, 6, 7); 
  sum+=this.funclength(...arr); 
  
  return sum;
}

f = function(): string { 
  return "hello anymous";
} 

functionAnonymous(): string {
 return this.f();
}


func = function(x,y): number{ 
  return x*y 
}; 
functionAnonymousParam(): string {
  const a=10, b=20;
  return 'The product of '+a+' and '+b+ ' is  --  ' +this.func(a,b);
}


funCons= new Function("x", "y", "return x*y;");   //function prototype
product() { 
   const a=10,b=20;
   const result = this.funCons(10,20); 
   return "The product of  "+a+'  and  '+b+' is  ---- '+result;
} 

functionConstractor(): string {
  return this.product();
}


factorial(num: number) { 
  if(num === 0) { 
     return 1; 
  } else { 
     return (num * this.factorial(num-1)  ) 
  } 
}
functionRecursion(): string {
  const a =6;
  return "The factorial of "+a+' is  --  '+this.factorial(a);
}



functionSelfExecutive(): string {
  (function() { 
    var msg = "Hello World" 
    // console.log(msg)
 })();
return "self executive function is executed inside another function/ or separately";
}

arrowSingle = (x)=>x*x;
arrowSingle1 = x=>x*x;         //parenthesis is optional
arrowSingle2 = ()=> 10*10;       //parenthesis is madatory if no param is there
add =(a,b)=> a+b;

arrowFunctionSingleLine(): string {
  const a =10;
  const sample1 = this.arrowSingle1(20);     //output 400
  const sample2 = this.arrowSingle2();       //output 100
  const sample4 = this.add(10,20);           //output 30
  //const sample3 = setTimeout(()=> 30*30,1000);       //output 900   here anonymous function will be used in callback function after every 1 second of setTimeOut()
  return "singleline arrow does not require {} <br>square of "+a+' is --- '+this.arrowSingle(a);
}


arrowMulti=(x)=> {
  const b=10;
  return x*x+b;
}

isEven = (n1) => {
  if(n1%2 == 0)
     return true;
  else
     return false;
}

arrowFunctionMultiLine(): string {
  const a =10;
  this.isEven(10);  //output - true
  return "multiline arrow function does require {} <br>expression of "+a+' is --- '+this.arrowMulti(a);
}


functionHoist(): string {
  "use strict";
  const result = hoist_function();  
  

  function hoist_function() { 
   return 'success';
  }

  /*const result1 = hoist_function1();  //ERROR TypeError: hoist_function1 is not a function
  var hoist_function1=  function() { 
   return 'success';
  }*/

  return 'function can be hoisted with or without strict mode however function expression cannot be hoisted'
}


iife1 = function(){
  let arr ="";
  (function() { 
    for(var x = 0;x<5;x++) { 
      arr+=x+' ';
    } 
 })();
 //console.log(x);      //x cannot be accessed outside of the iIEF block
 return arr;
}

iife2 = function(){
  let arr ="";
  var loop = function() { 
    for(var x = 0;x<10;x++) { 
      arr+=x+' ';
    } 
 }();

 //loop();              //this is not right way as this is not callable
 //console.log(x);      //x cannot be accessed outside of the iIEF block
 return arr;
}

iife(): string {
  // return this.iife1();
  return "Both styles of IIFE is checked -----   "+this.iife2();
}

generatorFunction(): string {
  "use strict";
  let sum = '';
function* rainbow() {  // the asterisk marks this as a generator 
   yield 'red'; 
   yield 'orange'; 
   yield 'yellow'; 
   yield 'green'; 
   yield 'blue'; 
   yield 'indigo'; 
   yield 'violet'; 
} 
for(let color of rainbow()) {   //yielded value of function will behave like an array
   sum+=color+'<br>';
} 
return sum;
}


generatorFunctionParam(): string {
  "use strict";
  let sum = '';

  function* askQuestion() {        // the asterisk marks this as a generator 
    const name = yield "What is your name?"; 
    const className = yield "Which class, do you study?"
    const sport = yield "What is your favorite sport?"; 
    const age = yield "What is your age?"; 
    return `${name}'s favorite sport is ${sport} and ${name} is a student of class ${className} with age ${age} years`; 
 }  
 const it = askQuestion();         //askQuestion {<closed>}    
 const name =it.next();             //{value: "What is your name?", done: false}
 const className = it.next('Ethan');    //{value: "Which class, do you study?", done: false}
 const sport = it.next('One');         //{value: "What is your favorite sport?", done: false}
 const age = it.next('Cricket');   //{value: "What is your age?", done: false}
 const finalResult = it.next(7);    //string interpolation
                                    //{value: "Ethan's favorite sport is Cricket and Ethan is a student of class One with age 7 years", done: true}

return finalResult.value;
}

arrowFunctionThis(): string {
  function Student(rollno,firstName,lastName) {
    let msg ='';
    this.rollno = rollno;
    this.firstName = firstName;
    this.lastName = lastName;
    this.fullNameUsingAnonymous = function(){
       setTimeout(function(){            //creates a new instance of this ,hides outer scope of this
         //console.log(this.firstName+ " "+this.lastName+" after 2 seconds");
       },2000);
    }
    this.fullNameWithoutSetTimeout = function(){
      return this.firstName+ " "+this.lastName;
    }
    this.fullNameUsingArrow = function(){
       setTimeout(()=>{                 //uses this instance of outer scope
        //console.log(this.firstName+ " "+this.lastName+" after 3 seconds");
       },3000);
    }

 };

 const s1 = new Student(101,'Mohammad','Mohtashim');
 //const withoutThis = s1.fullNameUsingAnonymous();         // undefined undefined     
 //const withThis = s1.fullNameUsingArrow();                 ////Mohammad Mohtashim    because of => operator
 const withOutsetTimeOut = s1.fullNameWithoutSetTimeout();   //Mohammad Mohtashim after 2 seconds

 return withOutsetTimeOut;
}


arrowFunctionMap(): string {
  const names = ['TutorialsPoint','Mohtashim','Bhargavi','Raja'];
  let msg = '';
   names.map((element,index)=> {
      msg+= 'index is '+index+' element value is :'+element+'<br><br>';
   });
   msg+='<br><br>';

   const obj = [
     {
    "_id": {
      "$oid": "5968dd23fc13ae04d9000001"
    },
    "product_name": "sildenafil citrate",
    "supplier": "Wisozk Inc",
    "quantity": 211,
    "unit_cost": "$10.47"
    }, 
    {
    "_id": {
      "$oid": "5968dd23fc13ae04d9000002"
    },
    "product_name": "Mountain Juniperus ashei",
    "supplier": "Keebler-Hilpert",
    "quantity": 292,
    "unit_cost": "$8.74"
    }, 
    {
    "_id": {
      "$oid": "5968dd23fc13ae04d9000003"
    },
    "product_name": "Dextromathorphan HBr",
    "supplier": "Schmitt-Weissnat",
    "quantity": 211,
    "unit_cost": "$20.53"
    }
];


obj.map((element,index)=> {
  element.quantity+=100;
  msg+= 'index is '+index+' element value is :'+JSON.stringify(element)+'<br><br>';
 });
 
 return msg;
}

arrowFunctionFilter(): string {
  let msg ='';
  const obj = [
    {
   "_id": {
     "$oid": "5968dd23fc13ae04d9000001"
   },
   "product_name": "sildenafil citrate",
   "supplier": "Wisozk Inc",
   "quantity": 261,
   "unit_cost": "$10.47"
   }, 
   {
   "_id": {
     "$oid": "5968dd23fc13ae04d9000002"
   },
   "product_name": "Mountain Juniperus ashei",
   "supplier": "Keebler-Hilpert",
   "quantity": 292,
   "unit_cost": "$8.74"
   }, 
   {
   "_id": {
     "$oid": "5968dd23fc13ae04d9000003"
   },
   "product_name": "Dextromathorphan HBr",
   "supplier": "Schmitt-Weissnat",
   "quantity": 211,
   "unit_cost": "$20.53"
   },
   {
    "_id": {
      "$oid": "5968dd23fc13ae04d9000008"
    },
    "product_name": "Dextromathorphan HBr",
    "supplier": "Schmitt-Weissnat",
    "quantity": 261,
    "unit_cost": "$20.53"
    }
];


const objfilter=obj.filter((element,index)=> {
  return element.quantity==211;
});

return JSON.stringify(objfilter);

}



}
