import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { Observable } from 'rxjs';
import {FormsComponent} from 'src/app/angular-basic/forms/forms.component';

export interface CanComponentDeactivate {
  confirm() : boolean;
 }

@Injectable()
export class ConfirmationGuard implements CanDeactivate <CanComponentDeactivate> {  //<Interface>
  canDeactivate(
    component: CanComponentDeactivate,  //interface name
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean > | Promise<boolean > | boolean {
    return component.confirm();
  }
  
}


