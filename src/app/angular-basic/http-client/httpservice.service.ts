import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';   //for http client
import {Observable} from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class HttpserviceService {
  private apiUrl ="/assets/mock-data/employee.json";
 
  private response;

  constructor(private httpClient: HttpClient) { }

  getData(): Observable <[]>{
    return this.httpClient.get<[]>(this.apiUrl);
  }
}
