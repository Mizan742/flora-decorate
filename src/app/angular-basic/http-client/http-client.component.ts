import { Component, OnInit } from '@angular/core';
import {HttpserviceService} from './httpservice.service';


@Component({
  selector: 'app-http-client',
  templateUrl: './http-client.component.html',
  styleUrls: ['./http-client.component.css']
})
export class HttpClientComponent implements OnInit {
  private data;
  private title="Angular http client"


  constructor(private service: HttpserviceService) { }

  ngOnInit() {
    this.service.getData().subscribe((response)=>{
      this.data = Array.from(Object.keys(response), k=>response[k]);
      console.log(this.data);
    });

  }

}
