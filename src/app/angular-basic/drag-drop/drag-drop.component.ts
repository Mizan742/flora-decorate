import { Component, OnInit } from '@angular/core';
import { AngularService } from '../angular-service';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';


@Component({
  selector: 'app-drag-drop',
  templateUrl: './drag-drop.component.html',
  styleUrls: ['./drag-drop.component.css']
})
export class DragDropComponent implements OnInit {
  private title = "Angular Drag and Drop CDK";
  private userData = [];

  constructor(private userDataService: AngularService) { }
  ngOnInit() {
    this.userDataService.getUserData().subscribe(response => {
      this.userData = response;
      console.log(response)

    });
  }

  onDrop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data,
        event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

 }


