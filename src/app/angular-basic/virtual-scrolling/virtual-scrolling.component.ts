import { Component, OnInit } from '@angular/core';
import {AngularService} from '../angular-service';

@Component({
  selector: 'app-virtual-scrolling',
  templateUrl: './virtual-scrolling.component.html',
  styleUrls: ['./virtual-scrolling.component.css']
})
export class VirtualScrollingComponent implements OnInit {
  private title="Angular virtual scrolling";
  private albumDetails =[];

  constructor(private servicePhotoData: AngularService) { }

  ngOnInit() {
    this.servicePhotoData.getPhotoData().subscribe(response=>{
      this.albumDetails = Array.from(Object.keys(response), k=>response[k]);
      console.log(this.albumDetails);
    });
  }



}
       