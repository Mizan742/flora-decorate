import {Component, OnInit} from '@angular/core';
import {Chart} from 'angular-highcharts';

@Component({
  selector: 'app-highchart',
  templateUrl: './highchart.component.html',
  styleUrls: ['./highchart.component.css'],
})


export class HighchartComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
  ////////////////////////////////////////
  chartHighLineChart = new Chart({
    chart: {
      type: 'line',
    },
    title: {
      text: 'Monthly Average Temperature',
    },
    subtitle: {
      text: 'Source: WorldClimate.com',
    },
    xAxis: {
      categories: [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Oct',
        'Nov',
        'Dec',
      ],
    },
    yAxis: {
      title: {
        text: 'Temperature (°C)',
      },
    },
    plotOptions: {
      line: {
        dataLabels: {
          enabled: true,
        },
        enableMouseTracking: false,
      },
    },
    series: [
      // {
      //   name: 'Tokyo',
      //   data: [
      //     7.0,
      //     6.9,
      //     9.5,
      //     14.5,
      //     18.4,
      //     21.5,
      //     25.2,
      //     26.5,
      //     23.3,
      //     18.3,
      //     13.9,
      //     9.6,
      //   ],
      // },
      // {
      //   name: 'London',
      //   data: [
      //     3.9,
      //     4.2,
      //     5.7,
      //     8.5,
      //     11.9,
      //     15.2,
      //     17.0,
      //     16.6,
      //     14.2,
      //     10.3,
      //     6.6,
      //     4.8,
      //   ],
      // },
    ],
  });

  ///////////////////////////////////////////
  chartHighBarChart = new Chart({
    chart: {
      type: 'bar',
    },
    title: {
      text: 'Historic World Population by Region',
    },
    subtitle: {
      text:
        'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>',
    },
    xAxis: {
      categories: ['Africa', 'America', 'Asia', 'Europe', 'Oceania'],
      title: {
        text: null,
      },
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Population (millions)',
        align: 'high',
      },
      labels: {
        overflow: 'justify',
      },
    },
    tooltip: {
      valueSuffix: ' millions',
    },
    plotOptions: {
      bar: {
        dataLabels: {
          enabled: true,
        },
      },
    },
    legend: {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'top',
      x: -40,
      y: 80,
      floating: true,
      borderWidth: 1,
      backgroundColor: '#FFFFFF',
      shadow: true,
    },
    credits: {
      enabled: false,
    },
    series: [
      // {
      //   name: 'Year 1800',
      //   data: [107, 31, 635, 203, 2],
      // },
      // {
      //   name: 'Year 1900',
      //   data: [133, 156, 947, 408, 6],
      // },
      // {
      //   name: 'Year 2000',
      //   data: [814, 841, 3714, 727, 31],
      // },
      // {
      //   name: 'Year 2016',
      //   data: [1216, 1001, 4436, 738, 40],
      // },
    ],
  });

  ////////////////////////////////////////
  chartStackedBar = new Chart({
    chart: {
      type: 'bar',
    },
    title: {
      text: 'Stacked bar chart',
    },
    xAxis: {
      categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas'],
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Total fruit consumption',
      },
    },
    legend: {
      reversed: true,
    },
    plotOptions: {
      series: {
        stacking: 'normal',
      },
    },
    series: [
      // {
      //   name: 'John',
      //   data: [5, 3, 4, 7, 2],
      // },
      // {
      //   name: 'Jane',
      //   data: [2, 2, 3, 2, 1],
      // },
      // {
      //   name: 'Joe',
      //   data: [3, 4, 4, 2, 5],
      // },
    ],
  });

  ///////////////////////////////////////

  chartpoint = new Chart({
    chart: {
      type: 'line',
    },
    title: {
      text: 'Linechart',
    },
    credits: {
      enabled: false,
    },
    series: [
      // {
      //   name: 'X axis',
      //   data: [1, 2, 3],
      // },
    ],
  });

  add() {
    this.chartpoint.addPoint(Math.floor(Math.random() * 10));
  }

  /////////////////////////////////////
}
 