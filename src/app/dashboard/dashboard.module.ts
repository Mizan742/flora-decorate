import {NgModule} from '@angular/core';

// import { ReactiveFormsModule } from '@angular/forms';
// import { CommonModule } from '@angular/common';

import {AmChartsModule} from '@amcharts/amcharts3-angular';
import {DashboardRoutingModule} from './dashboard.routing.module';
import {ChartModule} from 'angular-highcharts';

import {DashboardComponent} from './dashboard.component';
import {AmchartComponent} from './amchart/amchart.component';
import {HighchartComponent} from './highchart/highchart.component';
import {D3chartComponent} from './d3chart/d3chart.component';

import {KeysPipe} from './keys.pipe';
import {
  DoughnutChartComponent,
  PieChartComponent,
  BarChartComponent,
} from 'angular-d3-charts';

@NgModule({
  declarations: [
    DashboardComponent,
    AmchartComponent,
    HighchartComponent,
    D3chartComponent,
    DoughnutChartComponent,
    PieChartComponent,
    BarChartComponent,
    KeysPipe,
  ],
  imports: [DashboardRoutingModule, AmChartsModule, ChartModule],
})
export class DashboardModule {}
