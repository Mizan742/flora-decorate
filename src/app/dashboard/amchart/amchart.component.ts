import {Component, OnInit} from '@angular/core';
import {DisplayChartService} from '../display-chart.service';

@Component({
  selector: 'app-amchart',
  templateUrl: './amchart.component.html',
  styleUrls: ['./amchart.component.css'],
})
export class AmchartComponent implements OnInit {
  constructor(private amchartService: DisplayChartService) {}
  ngOnInit() {}

  ngAfterViewInit() {
    setTimeout(() => {
      this.amchartService.createBarChart();
    }, 0);

    setTimeout(() => {
      this.amchartService.createPieChart();
    }, 0);

    setTimeout(() => {
      this.amchartService.createHeatMap();
    }, 0);
  }
  ngOnDestroy() {
    this.amchartService.destroyBarChart();
    this.amchartService.destroyPieChart();
    this.amchartService.destroyHeatMap();
  }
}
