import {Injectable} from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';

@Injectable({
  providedIn: 'root',
})
export class ChartDataService {
  private healthData = [];
  constructor(private http: Http) {}
  getHealthdata() {
    return this.http
      .get('/assets/mock-data/health.json') // json files must be inside assets folder

      .map((response: Response) => {
        const data = response.json().data;

        return data;
      })
      .catch((error: Response) => {
        return Observable.throw('Something went wrong');
      });
  }

  getLitredata() {
    return this.http
      .get('/assets/mock-data/litre.json') // json files must be inside assets folder

      .map((response: Response) => {
        const data = response.json().data;

        return data;
      })
      .catch((error: Response) => {
        return Observable.throw('Something went wrong');
      });
  }

  getMapdata() {
    return this.http
      .get('/assets/mock-data/heatMap.json') // json files must be inside assets folder

      .map((response: Response) => {
        const data = response.json().data;

        return data;
      })
      .catch((error: Response) => {
        return Observable.throw('Something went wrong');
      });
  }

  getEmployeeData() {
    return this.http
      .get('/assets/mock-data/empLarge.json') // json files must be inside assets folder

      .map((response: Response) => {
        const data = response.json().Employees;

        return data;
      })
      .catch((error: Response) => {
        return Observable.throw('Something went wrong');
      });
  }
}
