import {Injectable} from '@angular/core';
import {AmChartsService, AmChart} from '@amcharts/amcharts3-angular';
import {ChartDataService} from './chart-data.service';

@Injectable({
  providedIn: 'root',
})
export class DisplayChartService {
  private barchart: AmChart;
  private pieChart: AmChart;
  private map: AmChart;
  private titleBarChart: string = 'General Health';
  private x: number = 0;

  private healthdata = this.chartDataDervice
    .getHealthdata()
    .subscribe(
      (responseHealthData) => (this.healthdata = responseHealthData),
      error => console.log(error)
    );

  private litreData = this.chartDataDervice
    .getLitredata()
    .subscribe(
      (responseLitreData) => (this.litreData = responseLitreData),
      error => console.log(error)
    );

  private mapData = this.chartDataDervice
    .getMapdata()
    .subscribe(
      (responseMapData) => (this.mapData = responseMapData),
      error => console.log(error)
    );

  constructor(
    private AmCharts: AmChartsService,
    private chartDataDervice: ChartDataService
  ) {}

  createBarChart() {
    this.barchart = this.AmCharts.makeChart('chartdivBarChart', {
      type: 'serial',
      theme: 'light',
      marginRight: 70,
      dataProvider: this.healthdata,
      valueAxes: [
        {
          axisAlpha: 0,
          axisThickness: 0,
          gridAlpha: 0,
          gridThickness: 0,
          position: 'left',
          title: this.titleBarChart,
        },
      ],
      startDuration: 1,

      graphs: [
        {
          balloonText: '<b>[[category]]: [[value]]</b>',
          labelPosition: 'middle',
          fillColorsField: 'color',
          fillAlphas: 0.9,
          lineAlpha: 0.2,
          type: 'column',
          valueField: 'nmPatient',
        },
      ],
      chartCursor: {
        categoryBalloonEnabled: false,
        cursorAlpha: 0,
        zoomable: false,
      },
      categoryField: 'state',
      categoryAxis: {
        gridPosition: 'start',
        labelRotation: 70,
      },
      export: {
        enabled: true,
      },
    });
  }

  destroyBarChart() {
    if (this.barchart) {
      this.AmCharts.destroyChart(this.barchart);
    }
  }

  createPieChart() {
    this.pieChart = this.AmCharts.makeChart('chartdivPie', {
      type: 'pie',
      theme: 'light',
      innerRadius: '40%',
      gradientRatio: [
        -0.4,
        -0.4,
        -0.4,
        -0.4,
        -0.4,
        -0.4,
        0,
        0.1,
        0.2,
        0.1,
        0,
        -0.2,
        -0.5,
      ],
      dataProvider: this.litreData,
      balloonText: '[[value]]',
      valueField: 'litres',
      titleField: 'country',
      balloon: {
        drop: true,
        adjustBorderColor: false,
        color: '#FFFFFF',
        fontSize: 16,
      },
      export: {
        enabled: true,
      },
    });
  }

  destroyPieChart() {
    if (this.pieChart) {
      this.AmCharts.destroyChart(this.pieChart);
    }
  }

  createHeatMap() {
    this.map = this.AmCharts.makeChart('chartdivUSMapHeat', {
      type: 'map',
      theme: 'light',
      colorSteps: 10,

      dataProvider: {
        map: 'usaLow',
        areas: this.mapData,
      },

      areasSettings: {
        autoZoom: true,
      },

      imagesSettings: {
        labelPosition: 'middle',
        labelFontSize: 8,
      },

      valueLegend: {
        right: 10,
        minValue: 'little',
        maxValue: 'a lot!',
      },
    });

    this.map.addListener('init', function() {
      // set up a longitude exceptions for certain areas
      let longitude = {
        'US-CA': -130,
        'US-FL': 120,
        'US-TX': 1,
        'US-LA': 40,
      };

      let latitude = {
        'US-AK': -85,
      };
    });
  }

  destroyHeatMap() {
    if (this.map) {
      this.AmCharts.destroyChart(this.map);
    }
  }
}
