import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {DashboardComponent} from './dashboard.component';
import {AmchartComponent} from './amchart/amchart.component'; 
import {HighchartComponent} from './highchart/highchart.component';
import {D3chartComponent} from './d3chart/d3chart.component';

const dashboardRoutes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {path: '', redirectTo: '/highchart', pathMatch: 'full'},
      {path: 'amchart', component: AmchartComponent},
      {path: 'd3chart', component: D3chartComponent},
      {path: 'highchart', component: HighchartComponent},
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(dashboardRoutes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
